# This file is a template, and might need editing before it works on your project.
FROM python:3.6

# Edit with mysql-client, postgresql-client, sqlite3, etc. for your needs.
# Or delete entirely if not needed.
RUN apt-get update \
    && apt-get install -y --no-install-recommends \
        postgresql-client \
    && rm -rf /var/lib/apt/lists/*

WORKDIR /usr/src/app

COPY requirements.txt /usr/src/app/
RUN pip install --no-cache-dir -r requirements.txt

COPY . /usr/src/app

# For Django
EXPOSE 8000
CMD ["python", "manage.py", "runserver", "0.0.0.0:8000"]

# For some other command
# CMD ["python", "app.py"]

def FizzBuzz(i,s,m):
    for idx in range(len(i)):
        tmp=i[idx]
        arg=idx
        for j in range(idx,len(i)):
            if tmp>i[j]:
                tmp=i[j]
                arg=j
            i[arg]=i[idx] #ここで段落を一つ間違えました
            i[idx]=tmp
            tmp_s=s[idx]
            s[idx]=s[arg]
            s[arg]=tmp_s

    for idx in range(len(i)):
        if m%i[idx]==0:
            print(s[idx])
    print(m)

f=open('input.txt')
list=f.readlines()
i,s=[],[]
for idx in range(len(list)-1):
    spl=list[idx].split(":")
    i.append(int(spl[0]))
    s.append(spl[1].split('\n')[0])
m=int(list[-1])
FizzBuzz(i,s,m)